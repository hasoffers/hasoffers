<?php

namespace HasOffer\Common\Message;

interface ResponseInterface
{
    public function isSuccessful();
}