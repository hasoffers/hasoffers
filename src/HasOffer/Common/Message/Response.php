<?php

namespace HasOffer\Common\Message;
use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\parse_query;

/**
 * Class Response
 * @package HasOffer\Common\Message
 */
class Response implements ResponseInterface
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var Client
     */
    private $request;

    /**
     * Response constructor.
     * @param $request
     * @param $data
     */
    public function __construct($request, $data)
    {
        $this->request = $request;
        $this->response = $data;
        $this->data = $this->payloadToArray($data);
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->data['success'] === 'true' ? true : false;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param $payload
     * @return array
     */
    private function payloadToArray($payload)
    {
        $payload = preg_replace('/;[^\w+]?/', '&', $payload);

        return parse_query($payload);
    }
}
