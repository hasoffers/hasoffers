<?php

namespace HasOffer;

use GuzzleHttp\Client;
use HasOffer\Postback\Goal;
use HasOffer\Postback\Offer;

class Postback
{
    /**
     * @var
     */
    private $params;

    /**
     * Postback constructor.
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * @param string $id
     * @param array $parameters
     * @return Offer
     */
    public function offer($id, array $parameters = [])
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->getEndpoint($parameters),
        ]);

        $offer = Offer::create($id, $parameters);
        $offer->setClient($client);

        return $offer;
    }

    /**
     * @param array $parameters
     * @return string
     */
    private function getEndpoint(array $parameters = [])
    {
        return sprintf('http://click.%s.com', $this->params['network_id']);
    }
}