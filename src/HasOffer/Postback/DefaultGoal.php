<?php

namespace HasOffer\Postback;

class DefaultGoal extends Goal
{
    /**
     * @return string
     */
    public function getGoalUrl()
    {
        return 'aff_lsr';
    }
}