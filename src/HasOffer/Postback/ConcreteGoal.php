<?php

namespace HasOffer\Postback;

class ConcreteGoal extends Goal
{
    /**
     * @var string
     */
    private $goal;

    /**
     * ConcreteGoal constructor.
     *
     * @param string $offer
     * @param string $goal
     */
    public function __construct($offer, $goal)
    {
        parent::__construct($offer);

        $this->goal = $goal;
    }

    /**
     * @return string
     */
    public function getGoalUrl()
    {
        return 'aff_goal';
    }

    /**
     * @param string $transactionId
     * @return array
     */
    protected function createGoalQuery($transactionId)
    {
        return array_merge([
                'a' => 'lsr',
                'goal_id' => $this->goal,
            ],
            parent::createGoalQuery($transactionId)
        );
    }
}