<?php

namespace HasOffer\Postback;

use GuzzleHttp\Client;
use HasOffer\Common\Message\Response;
use HasOffer\Common\Message\ResponseInterface;

/**
 * Class Goal
 * @package HasOffer\Postback
 */
abstract class Goal
{
    /**
     * @var string
     */
    protected $offer;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Goal constructor.
     * @param string $offer
     */
    public function __construct($offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return string
     */
    abstract function getGoalUrl();

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @param string $transaction
     * @return ResponseInterface
     */
    public function convert($transaction)
    {
        $client = $this->createClient();

        $httpResponse = $client->request('GET', $this->getGoalUrl(), [
            'query' => $this->createGoalQuery($transaction)
        ]);

        return $this->createResponse((string) $httpResponse->getBody());
    }

    /**
     * @param string $transactionId
     * @return array
     */
    protected function createGoalQuery($transactionId)
    {
        return [
            'offer_id' => $this->offer,
            'transaction_id' => $transactionId
        ];
    }

    /**
     * @param mixed $data
     * @return Response
     */
    protected function createResponse($data)
    {
        return new Response($this, $data);
    }

    /**
     * @return Client
     */
    private function createClient()
    {
        return $this->client;
    }
}