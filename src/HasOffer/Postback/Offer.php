<?php

namespace HasOffer\Postback;

/**
 * Class Offer
 * @package HasOffer\Postback
 */
class Offer
{
    /**
     * @param string $offerId
     * @param array $parameters
     * @return ConcreteGoal|DefaultGoal
     */
    static public function create($offerId, array $parameters = [])
    {
        if (array_key_exists('goal_id', $parameters)) {
            return new ConcreteGoal($offerId, $parameters['goal_id']);
        }

        return new DefaultGoal($offerId);
    }
}