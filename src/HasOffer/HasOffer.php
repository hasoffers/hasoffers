<?php

namespace HasOffer;

/**
 * Class HasOffer
 * @package HasOffer
 */
class HasOffer
{
    /**
     * @var string
     */
    protected $networkId;

    /**
     * @var string
     */
    protected $token;

    /**
     * HasOffer constructor.
     * @param string $networkId
     * @param string $token
     */
    public function __construct($networkId, $token)
    {
        $this->networkId = $networkId;
        $this->token = $token;
    }

    /**
     * @return Postback
     */
    public function postback()
    {
        return new Postback([
            'network_id' => $this->networkId,
        ]);
    }
}