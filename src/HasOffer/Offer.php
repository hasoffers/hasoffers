<?php

namespace HasOffer;

use GuzzleHttp\Client;

class Offer
{
    protected $id;
    protected $transactionId;

    public function __construct($id)
    {
        $this->client = new Client;

        $this->id = $id;
    }

    public function transaction($id)
    {
        $this->transactionId = $id;

        return $this;
    }

    /**
     * @param null $goalId If null, then act on default goal
     * @return Goal
     * @throws \Exception
     */
    public function goal($goalId = null)
    {
        if (empty($this->transactionId)) {
            throw new \Exception('No transaction id found.');
        }

        if (null === $goalId) {
            $path = 'aff_lsr';
            $query = [
                'offer_id' => $this->id,
                'transaction_id' => $this->transactionId
            ];
        } else {
            $path = 'aff_goal';
            $query = [
                'a' => 'lsr',
                'goal_id' => $goalId,
                'transaction_id' => $this->transactionId
            ];
        }

//        Log::debug('Sending goal', [
//            'url' => config('hasoffers.endpoint') . '/' . $path,
//            'query' => $query
//        ]);

        return $this->client->request(
            'GET',
            config('hasoffers.endpoint') . '/' . $path,
            ['query' => $query]
        );
    }
